# Symfony demo #

## proyecto en local ##
![Proyecto en local](/img/project_home.png)

## proyecto en labs ##
![Proyecto en labs](/img/symfony_labs.png)


### Referencia guia symfony ###
[Referencia guia symfony](https://symfony.com/doc/3.4/setup/web_server_configuration.html)

## Errores ##
### en la documentacion no explica los permisos que se ha de dar a los archivos. ###
### tampoco explica que el usuario de la carpeta no debe ser root ###
## Puntos dificiles ##
### la configuracion del document root al ser apache 2.4 no es suficiente con poner solo "Require all granted" ###

## Diferencia entre local y labs ##
### La diferencia esta en que en local tenemos toda la libertat de configurar o crear un virtual host, en cambio en el labs tenemos que configurar y crear un .htdocs###

### proyecto en el servidor labs ##
[proyecto en el servidor labs](http://labs.iam.cat/~a14juaramlag/symfony-demo/)